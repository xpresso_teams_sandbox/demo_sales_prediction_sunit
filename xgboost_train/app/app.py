"""
This is the implementation of training for rossman store sales prediction
"""

__author__ = "Naveen Sinha"

import pickle
import pandas as pd
import numpy as np
import os
import sys

from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from xgboost.sklearn import XGBRegressor
import scipy.stats as st

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger


logging = XprLogger("rossman_sales_project")


class XGBoostTraining(AbstractPipelineComponent):
    def __init__(self, base_folder="/data/kaggle_dataset"):

        super().__init__(name="XgBoost_Train")
        self.base_folder = base_folder
        self.combined_train_data = None
        self.features_train = None
        self.features_test = None
        self.labels_train = None
        self.labels_test = None

    def load(self):
        self.combined_train_data = pd.read_csv(
            os.path.join(self.base_folder,
                         "combined_train.csv"))

        # create X and y
        combined_train_data1 = self.combined_train_data.sample(frac=0.2)
        logging.info(combined_train_data1.head())
        feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                        'NewAssortment', 'NewStoreType']
        X = combined_train_data1[feature_cols]
        y = combined_train_data1.Sales
        y1 = combined_train_data1.Customers
        self.features_train, self.features_test, self.labels_train, self.labels_test = train_test_split(
            X, y, test_size=0.3, random_state=42)
        for col in feature_cols:
            logging.info(self.features_train[col].head())
            if np.isnan(self.features_train[col]).any():
                self.features_train.fillna(0, inplace=True)
            if np.isnan(self.features_test[col]).any():
                self.features_test.fillna(0, inplace=True)
            logging.info(np.isfinite(self.features_train).all())
            logging.info(np.isfinite(self.features_test).all())
        logging.info(self.labels_train.head())
        logging.info(np.isfinite(self.labels_train).all())

    def start(self, run_name):
        super().start(xpresso_run_name=run_name)
        logging.info("Training decision tree regressor")
        logging.info(self.features_train.head())

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)

        params = {
            "n_estimators": st.randint(3, 40),
            "max_depth": st.randint(3, 40),
            "learning_rate": st.uniform(0.05, 0.4),
            "colsample_bytree": one_to_left,
            "subsample": one_to_left,
            "gamma": st.uniform(0, 10),
            "reg_alpha": from_zero_positive,
            "min_child_weight": from_zero_positive,
        }
        temp_status = {"status": "Creating XGBRegressor"}
        temp_status.update({key: str(value) for key, value in params.items()})
        self.report_status(status=temp_status)
        mdl = XGBRegressor()
        model = RandomizedSearchCV(mdl, params, n_jobs=1, verbose=10)

        logging.info("Start training")
        model.fit(self.features_train, self.labels_train)
        self.report_status(status={"status": "Training Started"})
        logging.info("Training Completed")
        logging.info("Calculating accuracy")
        logging.info(self.features_test.shape)
        self.report_status(status={"status": "Calculating Accuracy"})
        r2_score_train = r2_score(self.labels_train,
                                  model.predict(self.features_train))
        r2_score_test = r2_score(self.labels_test,
                                 model.predict(self.features_test))
        logging.info("R2Score Train:{} R2Score Test: {}".format(r2_score_train,
                                                         r2_score_test))
        self.report_status(status={"status": "Accuracy Calculated",
                                   "accuracy": r2_score_test,
                                   "accuracy_train": r2_score_train,
                                   })
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        with open(os.path.join(self.OUTPUT_DIR, "xgboost.pkl"), "wb") as model_fs:
            pickle.dump(model, model_fs)
        logging.info("Saved model... load model")

        super().completed(push_exp=True)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py
    trainer = XGBoostTraining(base_folder="/data/kaggle_dataset")
    trainer.load()
    if len(sys.argv) >= 2:
        trainer.start(run_name=sys.argv[1])
    else:
        trainer.start(run_name="")

